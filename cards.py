'''
This module allows the user to generate and manipulate a Deck of Cards by shuffling, sorting,
drawing and showing the cards. These operations assist in playing a simple 3-card-draw game.
'''
import random
import sys


class Card: #pylint: disable=too-few-public-methods
    '''
    Cards have a suit and a number, for playing games.
    Precedence is set for a card when a Deck is created, to make sorting easier.
    '''
    def __init__(self, number, suit, precedence, points):
        self.number = number
        self.suit = suit
        self.precedence = precedence
        self.points = points

    def __str__(self):
        return "{} of {}".format(self.number, self.suit)


class Deck:
    '''A Deck is initialized with 52 cards and is shuffled (cards are in random order).'''
    def __init__(self):
        '''
        Insert a new Card into the deck for every suit / number combination.
        cards[0] is the bottom of the deck. cards[-1] is the top of the deck.
        Precedence / sorting operations assume the order of elements
        in suits and numbers lists.
        '''
        self.suits = [
            {"suit": "clubs", "points": 4}, {"suit": "hearts", "points": 3},
            {"suit": "diamonds", "points": 2}, {"suit": "spades", "points": 1}
        ]
        self.numbers = [
            {"number": "2", "points": 2}, {"number": "3", "points": 3},
            {"number": "4", "points": 4}, {"number": "5", "points": 5},
            {"number": "6", "points": 6}, {"number": "7", "points": 7},
            {"number": "8", "points": 8}, {"number": "9", "points": 9},
            {"number": "10", "points": 10}, {"number": "jack", "points": 11},
            {"number": "queen", "points": 12}, {"number": "king", "points": 13},
            {"number": "ace", "points": 14}
        ]
        cards = []
        precedence = 0
        for suit in self.suits:
            for number in self.numbers:
                points = suit["points"] + number["points"]
                card = Card(number["number"], suit["suit"], precedence, points)
                cards.append(card)
                precedence += 1
        self.cards = cards
        self.shuffle()

    def draw(self):
        '''Draw the top card of the deck and remove it from self.cards.'''
        try:
            card = self.cards[-1]
            del self.cards[-1]
            return card
        except IndexError as e: #pylint: disable=invalid-name
            raise IndexError(e) from e

    def shuffle(self):
        '''Shuffle Deck.cards into a new random order.'''
        random.shuffle(self.cards)

    def sort(self):
        '''Sort by the Card.precedence attribute added when the cards are created.'''
        self.cards.sort(key=lambda x: x.precedence)

    def show(self):
        '''Show every card in the deck.'''
        for card in self.cards:
            print(card)


class Game:
    '''
    Game has a deck and tracks player's turns and hands while a game is being played.
    the score() method calculates the scores and the winner when the game has ended.
    '''
    def __init__(self):
        self.turn_count = 1
        self.player1_hand = []
        self.player2_hand = []
        self.player1_score = 0
        self.player2_score = 0
        self.deck = Deck()

    def play(self):
        '''Play the game for three turns by drawing cards for Player1 and Player2.'''
        while self.turn_count <= 3:
            p1_card = self.deck.draw()
            p2_card = self.deck.draw()
            self.player1_hand.append(p1_card)
            self.player2_hand.append(p2_card)
            print("Turn {}:".format(self.turn_count))
            print("Player 1 drew {}".format(p1_card))
            print("Player 2 drew {}".format(p2_card))
            self.turn_count += 1

        print("\nGame complete: ")
        print("Player 1 hand: {}, {}, {}".format(
            self.player1_hand[0],
            self.player1_hand[1],
            self.player1_hand[2]
        ))
        print("Player 2 hand: {}, {}, {}".format(
            self.player2_hand[0],
            self.player2_hand[1],
            self.player2_hand[2]
        ))

    def score(self):
        '''Calculate scores for Player1 and Player2 and determine the winner.'''
        self.player1_score = sum([i.points for i in self.player1_hand])
        self.player2_score = sum([i.points for i in self.player2_hand])
        print("Player 1 score: {}".format(self.player1_score))
        print("Player 2 score: {}".format(self.player2_score))
        if self.player1_score > self.player2_score:
            return "Player 1 wins! Sorry Player 2."
        return "Player 2 wins! Sorry player 1."


def loop():
    '''Loop choices and accept user input until user quits the program.'''
    deck = Deck()
    choices = {
        "1": "Get a new deck of cards",
        "2": "Shuffle the deck",
        "3": "Sort the deck",
        "4": "Draw a card from the deck",
        "5": "Show the deck",
        "6": "Play a game",
        "7": "Exit"
    }

    while True:
        print()
        for i in choices:
            print("{}: {}".format(i, choices[i]))

        choice = input()

        if choice == "1":
            deck = Deck()
            print("New deck issued")
        elif choice == "2":
            deck.shuffle()
            print("Deck shuffled")
        elif choice == "3":
            deck.sort()
            print("Deck sorted")
        elif choice == "4":
            card = deck.draw()
            print("Card drawn: {}".format(card))
        elif choice == "5":
            deck.show()
        elif choice == "6":
            game = Game()
            game.play()
            score = game.score()
            print(score)
        elif choice == "7":
            sys.exit("Goodbye!")


if __name__ == "__main__":
    loop()
