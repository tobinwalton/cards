import pytest
from cards import Deck, Game


@pytest.fixture
def deck():
    '''Return a Deck class fixture'''
    return Deck()


@pytest.fixture
def game():
    '''Return a Game class fixture'''
    return Game()


class TestDeck:
    '''Test Deck class methods'''
    def test_card_count(self, deck):
        '''Check that the Deck has the required no. of cards (52)'''
        assert len(deck.cards) == 52

    def test_card_suits_cound(self, deck):
        '''Check that the Deck has the required no. of suits (4)'''
        assert len(deck.suits) == 4

    def test_card_types_count(self, deck):
        '''Check that the Deck has the required no. of card types (13)'''
        assert len(deck.numbers) == 13

    def test_deck_len_after_draw(self, deck):
        '''Check that the Deck is missing a card after drawing once'''
        deck.draw()
        assert len(deck.cards) == 51

    def test_card_missing_after_draw(self, deck):
        '''Check that the Deck is missing precisely the card that is drawn, after drawing once.'''
        card = deck.draw()
        assert card not in deck.cards

    def test_deck_error_if_no_cards(self, deck):
        '''Check that an IndexError is raised if the Deck contains no more cards.'''
        while len(deck.cards) > 0:
            deck.draw()
        with pytest.raises(IndexError):
            deck.draw()

    def test_shuffle(self, deck):
        '''Check that a shuffled Deck.cards is not the same as an unshuffled Deck.cards'''
        cards = deck.cards[:]
        deck.shuffle()
        assert cards != deck.cards


class TestGame():
    '''Test Game class methods'''
    def test_play_turn_count(self, game):
        '''Check that the allotted number of turns have passed after a Game is played'''
        game.play()
        assert game.turn_count == 4

    def test_play_player1_hand(self, game):
        '''Check that player1's hand contains 3 cards after Game is played'''
        game.play()
        assert len(game.player1_hand) == 3

    def test_play_player2_hand(self, game):
        '''Check that player2's hand contains 3 cards after Game is played'''
        game.play()
        assert len(game.player2_hand) == 3

    def test_play_deck_size(self, game):
        '''Check that 6 cards are missing from the the Deck after Game is played'''
        game.play()
        assert len(game.deck.cards) == 46

    def test_play_hands_not_in_deck(self, game):
        '''
        Check that the exact cards in the player's hands
        are missing from the Deck after Game is played
        '''
        game.play()
        hands = set()
        hands.update(set(game.player1_hand))
        hands.update(set(game.player2_hand))
        shared = hands.intersection(set(game.deck.cards))
        assert len(shared) == 0

    def test_score(self, game):
        '''Check that the correct winner is announced after the score is calculated'''
        game.play()
        score = game.score()
        if game.player1_score > game.player2_score:
            assert score == "Player 1 wins! Sorry Player 2."
        else:
            assert score == "Player 2 wins! Sorry player 1."
