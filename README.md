# Cards
"Cards" is a small card-game written in Python that demonstrates OO techniques and unit testing.
Pylint is used to govern programming style, and all modules, methods and functions are documented with docstrings.

The Pytest library is used for unit testing.

## Setup:
- Python3 and pip3 are assumed to be installed.
- run `pip3 install -r requirements.txt` or install pytest directly.

## Use:
- run `python cards.py` to use the program.
- the program is interactive and will provide instructions for playing the game and / or manipulating a virtual deck of cards.
- run `pylint` to exercise the unit-test suite.
